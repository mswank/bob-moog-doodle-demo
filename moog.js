// Copyright 2012 Google Inc. All Rights Reserved.
//
// Modified by Matthew Swank in 2015 to work on stock web browsers
// without the use of closure and the google libraries.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


(function() {
    /**
     * Connection interface for compound Web Audio nodes.
     * @interface
     */
    var CompoundAudioNode = function() {};


    /**
     * Gets the source node for this compound node.
     * @return {!AudioNode} The input/source node for this compound node.
     */
    CompoundAudioNode.prototype.getSourceNode = function() {};


    /**
     * Connects this compound node to a destination audio node.
     * @param {!AudioNode} destination The destination to connect this node to.
     */
    CompoundAudioNode.prototype.connect = function(destination) {};


    /**
     * Disconnects this compound node from any destination audio node.
     */
    CompoundAudioNode.prototype.disconnect = function() {
    };
    
    /**
     * A dynamic low pass filter emulator.
     * @param {!AudioContext} audioContext Audio context to which this oscillator
     *     will be bound.
     * @param {number} cutoffFrequency The cutoff frequency above which the
     *     frequencies are attenuated.
     * @param {number} contour Amount of contour to apply to the filter envelope.
     * @param {number} attackTime Initial attack time in seconds.
     * @param {number} decayTime Initial decay time in seconds.
     * @param {number} sustainLevel Initial sustain level [0..1].
     * @constructor
     * @implements {CompoundAudioNode}
     */
    var LowPassFilter = function(
	audioContext, cutoffFrequency, contour, attackTime, decayTime,
	sustainLevel) {
	/**
	 * Audio context in which this filter operates.
	 * @type {!AudioContext}
	 * @private
	 */
	this.audioContext_ = audioContext;

	/**
	 * The low pass filter Web Audio node.
	 * @type {!BiquadFilterNode}
	 * @private
	 */
	this.lowPassFilterNode_ = audioContext.createBiquadFilter();
	this.lowPassFilterNode_.type = 'lowpass';

	/**
	 * Frequency above which sound should be progressively attenuated.
	 * @type {number}
	 * @private
	 */
	this.cutoffFrequency_ = cutoffFrequency;

	/**
	 * The amount of 'contour' to add to the low pass filter ADS (attack, decay,
	 * sustain) envelope.  Contour is essentially a variable cutoff frequency
	 * coefficient. See EnvelopeGenerator for an explanation of how
	 * ADS envelopes work.
	 * @type {number}
	 * @private
	 */
	this.contour_ = contour;

	/**
	 * The duration of the attack phase in seconds.
	 * @type {number}
	 * @private
	 */
	this.attackTime_ = attackTime;

	/**
	 * The duration of the decay phase in seconds.
	 * @type {number}
	 * @private
	 */
	this.decayTime_ = decayTime;

	/**
	 * The sustain frequency coefficient.
	 * @type {number}
	 * @private
	 */
	this.sustainLevel_ = sustainLevel;

	this.resetContourEnvelope_();
    };


    /**
     * Initiates the attack phase of the contour envelope (e.g., when a key is
     * pressed).
     */
    LowPassFilter.prototype.startAttack = function() {
	var now = this.audioContext_.currentTime;

	this.lowPassFilterNode_.frequency.cancelScheduledValues(now);

	this.lowPassFilterNode_.frequency.value = this.cutoffFrequency_;
	this.lowPassFilterNode_.frequency.setValueAtTime(this.cutoffFrequency_, now);

	var contourFrequency = this.contour_ * this.cutoffFrequency_;

	this.lowPassFilterNode_.frequency.exponentialRampToValueAtTime(
	    contourFrequency, now + this.attackTime_);
	this.lowPassFilterNode_.frequency.exponentialRampToValueAtTime(
	    this.cutoffFrequency_ +
		this.sustainLevel_ * (contourFrequency - this.cutoffFrequency_),
	    now + this.attackTime_ + this.decayTime_);
    };


    /**
     * Sets the filter cutoff frequency.
     * @param {number} cutoffFrequency The cutoff frequency above which the
     *     frequencies are attenuated.
     */
    LowPassFilter.prototype.setCutoffFrequency = function(
	cutoffFrequency) {
	this.cutoffFrequency_ = cutoffFrequency;
	this.resetContourEnvelope_();
    };


    /**
     * Sets the filter envelope contour.
     * @param {number} contour Amount of contour to apply to the filter envelope.
     */
    LowPassFilter.prototype.setContour = function(contour) {
	this.contour_ = contour;
	this.resetContourEnvelope_();
    };


    /**
     * Sets attack phase duration.
     * @param {number} time Duration of the attack phase in seconds.
     */
    LowPassFilter.prototype.setContourAttackTime = function(time) {
	this.attackTime_ = time;
	this.resetContourEnvelope_();
    };


    /**
     * Sets decay phase duration.
     * @param {number} time Duration of the decay phase in seconds.
     */
    LowPassFilter.prototype.setContourDecayTime = function(time) {
	this.decayTime_ = time;
	this.resetContourEnvelope_();
    };


    /**
     * Sets the sustain level.
     * @param {number} level The sustain level, a number in the range [0, 1].
     */
    LowPassFilter.prototype.setContourSustainLevel = function(level) {
	this.sustainLevel_ = level;
	this.resetContourEnvelope_();
    };


    /**
     * Resets the contour envelope AudioParam using current LowPassFilter state.
     * @private
     */
    LowPassFilter.prototype.resetContourEnvelope_ = function() {
	this.lowPassFilterNode_.frequency.cancelScheduledValues(0);
	this.lowPassFilterNode_.frequency.value = this.cutoffFrequency_;
    };


    /** @inheritDoc */
    LowPassFilter.prototype.getSourceNode = function() {
	return this.lowPassFilterNode_;
    };


    /** @inheritDoc */
    LowPassFilter.prototype.connect = function(destination) {
	this.lowPassFilterNode_.connect(destination);
    };


    /** @inheritDoc */
    LowPassFilter.prototype.disconnect = function() {
	this.lowPassFilterNode_.disconnect();
    };
    
    
    /**
     * Master mixer for the multiple virtual synthesizers that may play back
     * simultaneously via the Sequencer.
     * @interface
     */
    var MasterMixerInterface = function() {};


    /**
     * Turns on the mixer.
     */
    MasterMixerInterface.prototype.turnOn = function() {};


    /**
     * Turns off the mixer.
     */
    MasterMixerInterface.prototype.turnOff = function() {};
    
    /**
     * Master mixer for the multiple virtual synthesizers that may play back
     * simultaneously via the Sequencer.
     * @param {!AudioContext} audioContext Audio context in which this mixer will
     *     operate.
     * @param {!Array.<!SynthesizerInterface>} synthesizers The
     *     synthesizers that feed into this mixer.
     * @param {!WideBandPassFilter} wideBandPassFilter The wide band
     *     pass filter used within this mixer.
     * @constructor
     * @implements {MasterMixerInterface}
     */
    var MasterMixer = function(
	audioContext, synthesizers, wideBandPassFilter) {
	/**
	 * The audio context in which this mixer operates.
	 * @type {!AudioContext}
	 * @private
	 */
	this.audioContext_ = audioContext;

	/**
	 * The synthesizers this mixer mixes.
	 * @type {!Array.<!Synthesizer>}
	 * @private
	 */
	this.synthesizers_ = synthesizers;

	/**
	 * Filter used to cut out really low/high frequencies.
	 * @type {!WideBandPassFilter}
	 * @private
	 */
	this.wideBandPassFilter_ = wideBandPassFilter;

	for (var i = 0; i < synthesizers.length; i++) {
	    synthesizers[i].connect(wideBandPassFilter.getSourceNode());
	}
    };


    /** @inheritDoc */
    MasterMixer.prototype.turnOn = function() {
	setTimeout((function() {
	    // NOTE: The audio pipeline is connected in a setTimeout to avoid a Chrome
	    // bug in which the audio will occasionally fail to initialize/play.
	    this.wideBandPassFilter_.connect(this.audioContext_.destination);
	}).bind(this), 0);
    };


    /** @inheritDoc */
    MasterMixer.prototype.turnOff = function() {
	this.wideBandPassFilter_.disconnect();
    };

    
    /**
     * Oscillator interface to be implemented in WebAudio and FlashAudio.
     * @interface
     */
    var OscillatorInterface = function() {};


    /**
     * Different methods in which an oscillator can fill an audio buffer.
     * @enum {number}
     */
    OscillatorInterface.FillMode = {
	// Clobber any pre-existing data in the buffer.
	CLOBBER: 0,
	// Add signal to the buffer.
	ADD: 1,
	// Add signal to the buffer and then do an even mix down.
	MIX: 2
    };


    /**
     * Different 'ranges' (i.e., octaves in synth lingo) in which an oscillator can
     * generate a tone.  'LO' is a special Moog-ism that generates sub-audio clicks
     * and pops.
     * @enum {number}
     */
    OscillatorInterface.Range = {
	LO: 0.0625,
	R32: 2,
	R16: 4,
	R8: 8,
	R4: 16,
	R2: 32
    };


    /**
     * Different wave forms that can be generated by an oscillator, each of which
     * has a different overtone spectrum leading to a different base timbre.
     * @enum {number}
     */
    OscillatorInterface.WaveForm = {
	TRIANGLE: 0,
	SAWANGLE: 1,
	RAMP: 2,
	REVERSE_RAMP: 3,
	SQUARE: 4,
	FAT_PULSE: 5,
	PULSE: 6
    };


    /**
     * Sets the volume on this oscillator.
     * @param {number} volume Volume in the range of [0, 1].
     */
    OscillatorInterface.prototype.setVolume = function(volume) {};


    /**
     * Sets the wave form generated by this oscillator.
     * @param {!OscillatorInterface.WaveForm} waveForm The wave form to
     *     use.
     */
    OscillatorInterface.prototype.setWaveForm = function(waveForm) {};


    /**
     * Sets the pitch bend on this oscillator.
     * @param {number} pitchBend Pitch bend amount in the range of [-1, 1].
     */
    OscillatorInterface.prototype.setPitchBend = function(pitchBend) {};


    /**
     * Sets the octave/range of this oscillator.
     * @param {!OscillatorInterface.Range} range The range of this
     *     oscillator.
     */
    OscillatorInterface.prototype.setRange = function(range) {};


    /**
     * Turns on keyboard pitch control.
     */
    OscillatorInterface.prototype.turnOnKeyboardPitchControl =
	function() {};


    /**
     * Turns off keyboard pitch control.
     */
    OscillatorInterface.prototype.turnOffKeyboardPitchControl =
	function() {};


    /**
     * Turns on frequency modulation.
     */
    OscillatorInterface.prototype.turnOnFrequencyModulation =
	function() {};


    /**
     * Turns off frequency modulation.
     */
    OscillatorInterface.prototype.turnOffFrequencyModulation =
	function() {};


    /**
     * Sets the modulation signal level.
     * @param {number} modulatorLevel If a modulator, how strong the modulator
     *     control signal should be [0..1].
     */
    OscillatorInterface.prototype.setModulatorLevel =
	function(modulatorLevel) {};


    /**
     * Turns on glide.
     */
    OscillatorInterface.prototype.turnOnGlide = function() {};


    /**
     * Turns off glide.
     */
    OscillatorInterface.prototype.turnOffGlide = function() {};


    /**
     * Sets the duration of gliding.
     * @param {number} time The time to glide between notes in seconds.
     */
    OscillatorInterface.prototype.setGlideDuration = function(time) {};


    /**
     * Sets the note this oscillator should generate.
     * @param {number} note Chromatic index of the note to be played relative to the
     *     beginning of the keyboard.
     */
    OscillatorInterface.prototype.setActiveNote = function(note) {};


    /**
     * Sets the attack time for this oscillator's envelope generator.
     * @param {number} attackTime The new attack time.
     */
    OscillatorInterface.prototype.setEnvelopeGeneratorAttackTime =
	function(attackTime) {};


    /**
     * Sets the decay time for this oscillator's envelope generator.
     * @param {number} decayTime The new decay time.
     */
    OscillatorInterface.prototype.setEnvelopeGeneratorDecayTime =
	function(decayTime) {};


    /**
     * Sets the sustain level for this oscillator's envelope generator.
     * @param {number} sustainLevel The new sustain level.
     */
    OscillatorInterface.prototype.setEnvelopeGeneratorSustainLevel =
	function(sustainLevel) {};
    
    
    /**
     * An ADSD envelope generator, used to control volume of a single synth note.
     *
     * See the Phase_ enum below for a description of how ADSD envelopes work.
     *
     * @param {number} sampleInterval How many seconds pass with each sample.
     * @param {number} attackTime Initial attack time in seconds.
     * @param {number} decayTime Initial decay time in seconds.
     * @param {number} sustainLevel Initial sustain level [0..1].
     * @constructor
     */
    var EnvelopeGenerator = function(
	sampleInterval, attackTime, decayTime, sustainLevel) {
	/**
	 * How many seconds pass with each sample.
	 * @type {number}
	 * @private
	 * @const
	 */
	this.SAMPLE_INTERVAL_ = sampleInterval;

	/**
	 * The amount by which the tone associated with this envelope should be
	 * scaled.  A number in the range [0, 1].
	 * @type {number}
	 * @private
	 */
	this.amplitudeCoefficient_ = 0;

	/**
	 * The current phase of the envelope.
	 * @type {!EnvelopeGenerator.Phase_}
	 * @private
	 */
	this.phase_ = EnvelopeGenerator.Phase_.INACTIVE;

	/**
	 * The duration of the attack phase in seconds.
	 * @type {number}
	 * @private
	 */
	this.attackTime_ = attackTime;

	/**
	 * How much the amplitude should be increased for each sample in the attack
	 * phase.
	 *
	 * NOTE: This variable and other 'step' variables can be computed from other
	 * EnvelopeGenerator state.  We explicitly store these values though since
	 * they are precisely the data needed in getNextAmplitudeCoefficient which is
	 * typically executed in a sound buffer filling loop and therefore performance
	 * critical.
	 * @type {number}
	 * @private
	 */
	this.attackStep_;

	/**
	 * The duration of the decay phase in seconds.
	 * @type {number}
	 * @private
	 */
	this.decayTime_ = decayTime;

	/**
	 * How much the amplitude should be decreased for each sample in the decay
	 * phase.
	 * @type {number}
	 * @private
	 */
	this.decayStep_;

	/**
	 * The sustain amplitude coefficient.
	 * @type {number}
	 * @private
	 */
	this.sustainLevel_ = sustainLevel;

	/**
	 * How much the amplitude should be decreased for each sample in the release
	 * phase.
	 * @type {number}
	 * @private
	 */
	this.releaseStep_;

	this.recomputePhaseSteps_();
    };


    /**
     * The different phases of an ADSD envelope.  When playing a key on a synth,
     * these phases are always executed in the order they're defined in this enum.
     *
     * @enum {number}
     * @private
     */
    EnvelopeGenerator.Phase_ = {
	// A linear ramp up from no volume (just before a key is struck) to maximum
	// volume.
	ATTACK: 0,
	// A linear ramp down from maximum volume to the sustain volume level.
	DECAY: 1,
	// Once the attack and decay phases have completed, the volume at which the
	// note is held until the key is released.
	SUSTAIN: 2,
	// A linear ramp down from the sustain volume to no volume after a key is
	// released.  Moog instruments uniquely reuse the decay time parameter for the
	// release time (hence the "ADSD" acronym instead of "ADSR").
	RELEASE: 3,
	// Meta state indicating that the envelope is not currently active.
	INACTIVE: 4
    };


    /**
     * Initiates the attack phase of the envelope (e.g., when a key is pressed).
     */
    EnvelopeGenerator.prototype.startAttack = function() {
	this.recomputePhaseSteps_();
	this.amplitudeCoefficient_ = 0;
	this.phase_ = EnvelopeGenerator.Phase_.ATTACK;
    };


    /**
     * Initiates the release phase of the envelope (e.g., when a key is lifted).
     */
    EnvelopeGenerator.prototype.startRelease = function() {
	if (this.phase_ == EnvelopeGenerator.Phase_.RELEASE) {
	    return;
	} else {
	    // Compute release step based on the current amplitudeCoefficient_.
	    if (this.decayTime_ <= 0) {
		this.releaseStep_ = 1;
	    } else {
		this.releaseStep_ =
		    this.amplitudeCoefficient_ * this.SAMPLE_INTERVAL_ / this.decayTime_;
	    }
	}
	this.phase_ = EnvelopeGenerator.Phase_.RELEASE;
    };


    /**
     * Gets the next amplitude coefficient that should be applied to the currently
     * playing note.  This method must be called (and applied, natch) for each
     * sample filled for the duration of a note.
     *
     * @return {number} An amplitude coefficient in the range [0, 1] that should be
     *     applied to the current sample.
     */
    EnvelopeGenerator.prototype.getNextAmplitudeCoefficient =
	function() {
	    switch (this.phase_) {
	    case EnvelopeGenerator.Phase_.ATTACK:
		this.amplitudeCoefficient_ += this.attackStep_;
		if (this.amplitudeCoefficient_ >= 1) {
		    this.amplitudeCoefficient_ = 1;
		    this.phase_ = EnvelopeGenerator.Phase_.DECAY;
		}
		break;
	    case EnvelopeGenerator.Phase_.DECAY:
		this.amplitudeCoefficient_ -= this.decayStep_;
		if (this.amplitudeCoefficient_ <= this.sustainLevel_) {
		    this.amplitudeCoefficient_ = this.sustainLevel_;
		    this.phase_ = EnvelopeGenerator.Phase_.SUSTAIN;
		}
		break;
	    case EnvelopeGenerator.Phase_.SUSTAIN:
		// Just stay at the sustain level until a release is signaled.
		break;
	    case EnvelopeGenerator.Phase_.RELEASE:
		this.amplitudeCoefficient_ -= this.releaseStep_;
		if (this.amplitudeCoefficient_ <= 0) {
		    this.amplitudeCoefficient_ = 0;
		    this.phase_ = EnvelopeGenerator.Phase_.INACTIVE;
		}
		break;
	    case EnvelopeGenerator.Phase_.INACTIVE:
		// Stay at 0, as set in the release transition (muted).
		break;
	    }

	    return this.amplitudeCoefficient_;
	};


    /**
     * Sets attack phase duration.
     * @param {number} time Duration of the attack phase in seconds.
     */
    EnvelopeGenerator.prototype.setAttackTime = function(time) {
	this.attackTime_ = time;
	this.recomputePhaseSteps_();
    };


    /**
     * Sets decay phase duration.
     * @param {number} time Duration of the decay phase in seconds.
     */
    EnvelopeGenerator.prototype.setDecayTime = function(time) {
	this.decayTime_ = time;
	this.recomputePhaseSteps_();
    };


    /**
     * Sets the sustain level.
     * @param {number} level The sustain level, a number in the range [0, 1].
     */
    EnvelopeGenerator.prototype.setSustainLevel = function(level) {
	this.sustainLevel_ = level;
	this.recomputePhaseSteps_();
    };


    /**
     * Updates phase step variables to reflect current EnvelopeGenerator state.
     * @private
     */
    EnvelopeGenerator.prototype.recomputePhaseSteps_ = function() {
	if (this.attackTime_ <= 0) {
	    this.attackStep_ = 1;
	} else {
	    this.attackStep_ = this.SAMPLE_INTERVAL_ / this.attackTime_;
	}

	if (this.decayTime_ <= 0) {
	    this.decayStep_ = 1;
	    this.releaseStep_ = 1;
	} else {
	    this.decayStep_ =
		(1 - this.sustainLevel_) * this.SAMPLE_INTERVAL_ / this.decayTime_;
	    this.releaseStep_ =
		this.sustainLevel_ * this.SAMPLE_INTERVAL_ / this.decayTime_;
	}
    };
    
    /**
     * A voltage controlled oscillator emulator built on the HTML5 Audio API.  This
     * is the root source of all sound in the Moog doodle.
     *
     * @param {!AudioContext} audioContext Audio context to which this oscillator
     *     will be bound.
     * @param {number} volume Initial volume level [0..1].
     * @param {!OscillatorInterface.WaveForm} waveForm Initial wave
     *     form.
     * @param {!OscillatorInterface.Range} range Initial pitch range.
     * @param {number} pitchBend Initial pitch bend [-1..1].
     * @param {boolean} isAcceptingKeyboardPitch Whether keyboard control is on.
     * @param {boolean} isFrequencyModulationOn Whether this oscillator should
     *     modulate its frequency using the modulator control signal.
     * @param {boolean} isModulator Whether this modulator should act as a modulator
     *     control signal in addition to an audio signal.
     * @param {number} modulatorLevel If a modulator, how strong the modulator
     *     control signal should be [0..1].
     * @param {boolean} isGlideOn Whether glide is on.
     * @param {number} glideTime Glide time in seconds.
     * @param {number} attackTime Initial attack time in seconds.
     * @param {number} decayTime Initial decay time in seconds.
     * @param {number} sustainLevel Initial sustain level [0..1].
     * @constructor
     * @implements {OscillatorInterface}
     */
    var Oscillator = function(
	audioContext, volume, waveForm, range, pitchBend, isAcceptingKeyboardPitch,
	isFrequencyModulationOn, isModulator, modulatorLevel, isGlideOn, glideTime,
	attackTime, decayTime, sustainLevel) {
	/**
	 * How many seconds pass with each sample.
	 * @type {number}
	 * @private
	 * @const
	 */
	this.SAMPLE_INTERVAL_ = 1 / audioContext.sampleRate;

	/**
	 * How many seconds we have advanced in the current cycle.
	 * @type {number}
	 * @private
	 */
	this.phase_ = 0.0;

	/**
	 * The amount of oscillator signal which is fed into the mixer.  Mapped onto
	 * the range [0.0, 1.0] where 0 means muted and 1.0 means full loudness.
	 * @type {number}
	 * @private
	 */
	this.volume_ = volume;

	/**
	 * Shape of the wave this oscillator generates.
	 * @type {!OscillatorInterface.WaveForm}
	 * @private
	 */
	this.waveForm_ = waveForm;

	/**
	 * Octave/range in which the oscillator functions.
	 * @type {!OscillatorInterface.Range}
	 * @private
	 */
	this.range_ = range;

	/**
	 * Pitch bend to apply on top of the base frequency on the range [-1.0, 1.0].
	 * If isAcceptingKeyboardPitch_ is true, -1/1 are a major 6th down/up; 0 is no
	 * pitch bend.  If isAcceptingKeyboardPitch_ is false, -1/1 are 3 octaves
	 * down/up; 0 is still no pitch bend.
	 * @type {number}
	 * @private
	 */
	this.pitchBend_ = pitchBend;

	/**
	 * Whether the oscillator pitch is controlled by keyboard signals.  If true,
	 * the pitch follows the keyboard as one would expect on a piano-like
	 * instrument.  If false, pressing any key on the keyboard will result in the
	 * same pitch (as set by range_ and pitchBend_).
	 * @type {boolean}
	 * @private
	 */
	this.isAcceptingKeyboardPitch_ = isAcceptingKeyboardPitch;

	/**
	 * Whether this oscillator should modulate its frequency using the modulator
	 * control signal.
	 * @type {boolean}
	 * @private
	 */
	this.isFrequencyModulationOn_ = isFrequencyModulationOn;

	/**
	 * Whether this modulator should act as a modulator control signal in addition
	 * to an audio signal.
	 * @type {boolean}
	 * @const
	 * @private
	 */
	this.IS_MODULATOR_ = isModulator;

	/**
	 * If this oscillator is also a modulator, how strong the modulator control
	 * signal should be [0..1].  0 fully attenuates the control signal; 1 fully
	 * applies it.
	 * @type {number}
	 * @private
	 */
	this.modulatorLevel_ = modulatorLevel;

	/**
	 * If this oscillator is also a modulator, a buffer for storing modulator
	 * control signal samples.  Null iff this oscillator does not act as a
	 * modulator.
	 * @type {Float32Array}
	 */
	this.modulatorSignal = null;

	/**
	 * Whether glide (portamento sliding between notes) is enabled.
	 * @type {boolean}
	 * @private
	 */
	this.isGlideOn_ = isGlideOn;

	/**
	 * Glide duration in seconds.
	 * @type {number}
	 * @private
	 */
	this.glideDuration_ = glideTime;

	/**
	 * How much to advanced the frequency per sample while gliding.  Null until
	 * the first tone is played.
	 * @type {?number}
	 * @private
	 */
	this.glideDelta_ = null;

	/**
	 * Current glide pitch.  Null until the first tone is played.
	 * @type {?number}
	 * @private
	 */
	this.currentGlidePitch_ = null;

	/**
	 * Target glide pitch.  Null until the first tone is played.
	 * @type {?number}
	 * @private
	 */
	this.targetGlidePitch_ = null;

	/**
	 * Chromatic index of the note to be played relative to the beginning of the
	 * keyboard.
	 * @type {number}
	 * @private
	 */
	this.activeNote_ = 0;

	/**
	 * Envelope generator that will shape the dynamics of this oscillator.
	 * @type {!EnvelopeGenerator}
	 */
	this.envelopeGenerator =
	    new EnvelopeGenerator(
		this.SAMPLE_INTERVAL_, attackTime, decayTime, sustainLevel);
    };


    /**
     * The base frequency (in hertz) from which other notes' frequencies are
     * calculated.  A low 'A'.
     * @type {number}
     * @const
     * @private
     */
    Oscillator.BASE_FREQUENCY_ = 55;


    /**
     * Frequency ratio of major sixth musical interval.
     * @type {number}
     * @const
     * @private
     */
    Oscillator.MAJOR_SIXTH_FREQUENCY_RATIO_ = 5 / 3;


    /**
     * Frequency ratio of a three octave musical interval.
     * @type {number}
     * @const
     * @private
     */
    Oscillator.THREE_OCTAVE_FREQUENCY_RATIO_ = 8;


    /**
     * The chromatic distance between keyboard notes (as passed to
     * getInstantaneousFrequency_) and low A.
     * @type {number}
     * @const
     * @private
     */
    Oscillator.NOTE_OFFSET_ = -4;


    /** @inheritDoc */
    Oscillator.prototype.setVolume = function(volume) {
	this.volume_ = volume;
    };


    /** @inheritDoc */
    Oscillator.prototype.setWaveForm = function(waveForm) {
	this.waveForm_ = waveForm;
    };


    /** @inheritDoc */
    Oscillator.prototype.setPitchBend = function(pitchBend) {
	this.pitchBend_ = pitchBend;
    };


    /** @inheritDoc */
    Oscillator.prototype.setRange = function(range) {
	this.range_ = range;
    };


    /** @inheritDoc */
    Oscillator.prototype.turnOnKeyboardPitchControl = function() {
	this.isAcceptingKeyboardPitch_ = true;
    };


    /** @inheritDoc */
    Oscillator.prototype.turnOffKeyboardPitchControl = function() {
	this.isAcceptingKeyboardPitch_ = false;
    };


    /** @inheritDoc */
    Oscillator.prototype.turnOnFrequencyModulation = function() {
	this.isFrequencyModulationOn_ = true;
    };


    /** @inheritDoc */
    Oscillator.prototype.turnOffFrequencyModulation = function() {
	this.isFrequencyModulationOn_ = false;
    };


    /** @inheritDoc */
    Oscillator.prototype.setModulatorLevel = function(modulatorLevel) {
	this.modulatorLevel_ = modulatorLevel;
    };


    /** @inheritDoc */
    Oscillator.prototype.turnOnGlide = function() {
	this.isGlideOn_ = true;
    };


    /** @inheritDoc */
    Oscillator.prototype.turnOffGlide = function() {
	this.isGlideOn_ = false;
    };


    /** @inheritDoc */
    Oscillator.prototype.setGlideDuration = function(time) {
	this.glideDuration_ = time;
    };


    /** @inheritDoc */
    Oscillator.prototype.setActiveNote = function(note) {
	this.activeNote_ = note;
    };


    /** @inheritDoc */
    Oscillator.prototype.setEnvelopeGeneratorAttackTime =
	function(attackTime) {
	    this.envelopeGenerator.setAttackTime(attackTime);
	};


    /** @inheritDoc */
    Oscillator.prototype.setEnvelopeGeneratorDecayTime =
	function(decayTime) {
	    this.envelopeGenerator.setDecayTime(decayTime);
	};


    /** @inheritDoc */
    Oscillator.prototype.setEnvelopeGeneratorSustainLevel =
	function(sustainLevel) {
	    this.envelopeGenerator.setSustainLevel(sustainLevel);
	};


    /**
     * Gets the instantaneous frequency that this oscillator ought to be generating.
     * @param {number} note Chromatic index of the note to be played relative to
     *     the beginning of the keyboard.
     * @return {number} The instantaneous frequency.
     * @private
     */
    Oscillator.prototype.getInstantaneousFrequency_ = function(note) {
	if (!this.isAcceptingKeyboardPitch_) {
	    note = 0;
	}

	return Oscillator.BASE_FREQUENCY_ *
	    this.range_ *
	    Math.pow(2, (note + Oscillator.NOTE_OFFSET_) / 12);
    };


    /**
     * Gets the pitch bend that should be applied to this oscillator.
     * @return {number} The frequency ratio that should be applied to the base
     *     pitch.
     * @private
     */
    Oscillator.prototype.getPitchBend_ = function() {
	var pitchBendScale = Oscillator.MAJOR_SIXTH_FREQUENCY_RATIO_;
	if (!this.isAcceptingKeyboardPitch_) {
	    pitchBendScale = Oscillator.THREE_OCTAVE_FREQUENCY_RATIO_;
	}
	var normalizedPitchBend =
		Math.abs(this.pitchBend_ * (pitchBendScale - 1)) + 1;
	return this.pitchBend_ >= 0 ? normalizedPitchBend : 1 / normalizedPitchBend;
    };


    /**
     * Gets the frequency this oscillator ought to generate after undergoing
     * optional glide.
     * @param {number} target The target frequency to move towards.
     * @return {number} The frequency this oscillator should generate with glide.
     * @private
     */
    Oscillator.prototype.getGlideFrequency_ = function(target) {
	if (!this.isGlideOn_ ||
	    this.currentGlidePitch_ === null ||
	    this.glideDuration_ <= 0 ||
	    Math.abs(this.currentGlidePitch_ - target) <=
	    Math.abs(this.glideDelta_)) {
	    this.currentGlidePitch_ = this.targetGlidePitch_ = target;
	    return target;
	}

	if (this.targetGlidePitch_ != target) {
	    var glideDurationInSamples = this.glideDuration_ / this.SAMPLE_INTERVAL_;
	    this.glideDelta_ =
		(target - this.currentGlidePitch_) / glideDurationInSamples;
	    this.targetGlidePitch_ = target;
	}

	this.currentGlidePitch_ += this.glideDelta_;
	return this.currentGlidePitch_;
    };


    /**
     * Gets the frequency this oscillator ought to generate after undergoing
     * optional frequency modulation.
     * @param {number} baseFrequency The base frequency to modulate.
     * @param {Float32Array} modulatorSignal Modulator signal buffer to apply to
     *     this oscillator.  If null, no modulation should be applied.
     * @param {number} index Sample index of the modulation signal to apply.
     * @return {number} The frequency this oscillator should generate with
     *     modulation.
     * @private
     */
    Oscillator.prototype.getModulationFrequency_ = function(
	baseFrequency, modulatorSignal, index) {
	if (!modulatorSignal || !this.isFrequencyModulationOn_) {
	    return baseFrequency;
	}
	// Linearly project modulation level [0..1] onto [0.5..2] (down/up an octave).
	// TODO: Re-evaluate this maximum frequency modulation range based on real
	// Moog behavior.
	return (modulatorSignal[index] * 0.75 + 1.25) * baseFrequency;
    };


    /**
     * Advances the oscillator's phase of play.  This helper function must be called
     * exactly once per sample.
     * @param {number} frequency The frequency being played.
     * @return {number} Progress made in the current cycle, projected on the range
     *     [0, 1].
     * @private
     */
    Oscillator.prototype.advancedPhase_ = function(frequency) {
	var cycleLengthInSeconds = 2 / frequency;
	if (this.phase_ > cycleLengthInSeconds) {
	    this.phase_ -= cycleLengthInSeconds;
	}
	var progressInCycle = this.phase_ * frequency / 2;
	this.phase_ += this.SAMPLE_INTERVAL_;
	return progressInCycle;
    };


    /**
     * Fills the passed audio buffer with the tone represented by this oscillator.
     * @param {!AudioProcessingEvent} e The audio process event object.
     * @param {Float32Array} modulatorSignal Modulator signal buffer to apply to
     *     this oscillator.  If null, no modulation will be applied.
     * @param {!OscillatorInterface.FillMode} fillMode How the
     *     oscillator should fill the passed audio buffer.
     * @param {number=} opt_mixDivisor Iff using FillMode.MIX, how much to mix down
     *     the buffer.  This should usually be the number of oscillators that have
     *     added data to the buffer.
     */
    Oscillator.prototype.fillAudioBuffer = function(
	e, modulatorSignal, fillMode, opt_mixDivisor) {
	var buffer = e.outputBuffer;
	//var left = buffer.getChannelData(1);
	var right = buffer.getChannelData(0);

	if (this.IS_MODULATOR_ && !this.modulatorSignal) {
	    this.modulatorSignal = new Float32Array(buffer.length);
	}

	var targetFrequency = this.getInstantaneousFrequency_(this.activeNote_);
	var pitchBend = this.getPitchBend_();
	var frequency = targetFrequency * pitchBend;

	var audioLevel, envelopeCoefficient, level, on, progressInCycle, ramp;
	for (var i = 0; i < buffer.length; ++i) {
	    envelopeCoefficient =
		this.envelopeGenerator.getNextAmplitudeCoefficient();
	    progressInCycle = this.advancedPhase_(
		pitchBend *
		    this.getModulationFrequency_(
			this.getGlideFrequency_(targetFrequency), modulatorSignal, i));
	    switch (this.waveForm_) {
	    case OscillatorInterface.WaveForm.TRIANGLE:
		level = 4 * ((progressInCycle > 0.5 ?
			      1 - progressInCycle : progressInCycle) - .25);
		break;
	    case OscillatorInterface.WaveForm.SAWANGLE:
		ramp = progressInCycle < 0.5;
		level = ramp ? (4 * progressInCycle - 1) : (-2 * progressInCycle + 1);
		break;
	    case OscillatorInterface.WaveForm.RAMP:
		level = 2 * (progressInCycle - 0.5);
		break;
	    case OscillatorInterface.WaveForm.REVERSE_RAMP:
		level = -2 * (progressInCycle - 0.5);
		break;
	    case OscillatorInterface.WaveForm.SQUARE:
		level = progressInCycle < 0.5 ? 1 : -1;
		break;
	    case OscillatorInterface.WaveForm.FAT_PULSE:
		level = progressInCycle < 1 / 3 ? 1 : -1;
		break;
	    case OscillatorInterface.WaveForm.PULSE:
		level = progressInCycle < 0.25 ? 1 : -1;
		break;
	    }

	    if (this.IS_MODULATOR_) {
		this.modulatorSignal[i] = level * this.modulatorLevel_;
	    }

	    audioLevel = level * this.volume_ * envelopeCoefficient;
	    if (fillMode == OscillatorInterface.FillMode.CLOBBER) {
		right[i] = audioLevel;
	    } else if (fillMode == OscillatorInterface.FillMode.ADD) {
		right[i] = right[i] + audioLevel;
	    } else {  // Otherwise FillMode.MIX.
		right[i] = (right[i] + audioLevel) / opt_mixDivisor;

		// In older versions of Chrome, Web Audio API always created two
		// channels even if you have requested monaural sound. However, this
		// is not the case in the newer (dev/canary) versions. This should
		// cover both.
		// if (left) {
		// 	left[i] = right[i];
		// }
	    }
	}
    };

    
    /**
     * A Moog-like synthesizer built on top of the HTML5 Web Audio API.
     * @interface
     */
    var SynthesizerInterface = function() {};


    /**
     * Signals that a key on the synthesizer has been pressed.
     * @param {number} note Chromatic index of the note to be played relative to
     *     the beginning of the keyboard.
     */
    SynthesizerInterface.prototype.setKeyDown = function(note) {};


    /**
     * Binds a callback to a key down event.
     * @param {function(number)} callback The function called when a key is pressed.
     */
    SynthesizerInterface.prototype.setKeyDownCallback =
	function(callback) {};


    /**
     * Signals that a key on the synthesizer has been released.  Since the
     * synthesizer is monophonic, no note parameter is necessary.
     */
    SynthesizerInterface.prototype.setKeyUp = function() {};


    /**
     * Increases/decreases the output gain.
     * @param {number} volume The output volume level.  A number in the range
     *     [0..1].
     */
    SynthesizerInterface.prototype.setVolume = function(volume) {};


    /**
     * Proxy to the synthesizer's low-pass filter setCutoffFrequency.
     * @param {number} cutoffFrequency The cutoff frequency above which the
     *     frequencies are attenuated.
     */
    SynthesizerInterface.prototype.setLpCutoffFrequency =
	function(cutoffFrequency) {};


    /**
     * Proxy to the synthesizer's low-pass filter setContour.
     * @param {number} contour Amount of contour to apply to the filter envelope.
     */
    SynthesizerInterface.prototype.setLpContour =
	function(contour) {};


    /**
     * Proxy to the synthesizer's low-pass filter setContourAttackTime.
     * @param {number} time Duration of the attack phase in seconds.
     */
    SynthesizerInterface.prototype.setLpContourAttackTime =
	function(time) {};


    /**
     * Proxy to the synthesizer's low-pass filter setContourDecayTime.
     * @param {number} time Duration of the decay phase in seconds.
     */
    SynthesizerInterface.prototype.setLpContourDecayTime =
	function(time) {};


    /**
     * Proxy to the synthesizer's low-pass filter setContourSustainLevel.
     * @param {number} level The sustain level, a number in the range [0, 1].
     */
    SynthesizerInterface.prototype.setLpContourSustainLevel =
	function(level) {};

    
    /**
     * A Moog-like synthesizer built on top of the HTML5 Web Audio API.
     * @param {!AudioContext} audioContext Audio context in which this synthesizer
     *     will operate.
     * @param {!Array.<!Oscillator>} oscillators The oscillators used
     *     within this synthesizer.
     * @param {!LowPassFilter} lowPassFilter The low pass filter used
     *     within this synthesizer.
     * @param {number} volume How much the gain should be adjusted.
     * @implements {CompoundAudioNode}
     * @implements {SynthesizerInterface}
     * @constructor
     */
    var Synthesizer = function(
	audioContext, oscillators, lowPassFilter, volume) {
	/**
	 * The audio context in which this synthesizer operates.
	 * @type {!AudioContext}
	 * @private
	 */
	this.audioContext_ = audioContext;

	/**
	 * Oscillators used to generate synthesizer sounds.
	 * @type {!Array.<!Oscillator>}
	 */
	this.oscillators = oscillators;

	/**
	 * Filter used to cut off high frequencies.
	 * @type {!LowPassFilter}
	 */
	this.lowPassFilter = lowPassFilter;

	/**
	 * The volume Web Audio node.
	 * @type {!AudioGainNode}
	 * @private
	 */
	this.volumeNode_ = audioContext.createGain();

	/**
	 * Frequency analyser node.
	 * @type {!RealtimeAnalyserNode}
	 */
	this.analyserNode = audioContext.createAnalyser();
	this.analyserNode.smoothingTimeConstant = 0.5;
	

	/**
	 * Volume/gain adjustment to apply to the output signal.
	 * @type {number}
	 * @private
	 */
	this.volume_;
	this.setVolume(volume);

	/**
	 * Web Audio JavaScript node used by oscillators to generate sound.
	 * @type {!JavaScriptAudioNode}
	 * @private
	 */
	this.jsAudioNode_ = audioContext.createScriptProcessor(Synthesizer.BUFFER_SIZE_, 1, 1);

	this.jsAudioNode_.onaudioprocess = this.fillAudioBuffer_.bind(this);
	this.jsAudioNode_.connect(this.lowPassFilter.getSourceNode());
	this.lowPassFilter.connect(this.volumeNode_);
	this.volumeNode_.connect(this.analyserNode);
    };


    /**
     * How many samples to fill at a time in a JavaScript audio node callback.  A
     * larger number here will decrease the chance of jitters/gaps at the expense of
     * control latency.
     * @type {number}
     * @const
     * @private
     */
    Synthesizer.BUFFER_SIZE_ = 1024;


    /** @inheritDoc */
    Synthesizer.prototype.setKeyDown = function(note) {
	this.oscillators.forEach(function(oscillator) {
	    oscillator.setActiveNote(note);
	    oscillator.envelopeGenerator.startAttack();
	});
	this.lowPassFilter.startAttack();
    };


    /** @inheritDoc */
    Synthesizer.prototype.setKeyDownCallback = function(callback) {
	// No-op, since this is only required on non-WebAudio browsers.
    };


    /** @inheritDoc */
    Synthesizer.prototype.setKeyUp = function() {
	this.oscillators.forEach(function(oscillator) {
	    oscillator.envelopeGenerator.startRelease();
	});
    };


    /** @inheritDoc */
    Synthesizer.prototype.setLpCutoffFrequency =
	function(cutoffFrequency) {
	    this.lowPassFilter.setCutoffFrequency(cutoffFrequency);
	};


    /** @inheritDoc */
    Synthesizer.prototype.setLpContour = function(contour) {
	this.lowPassFilter.setContour(contour);
    };


    /** @inheritDoc */
    Synthesizer.prototype.setLpContourAttackTime = function(time) {
	this.lowPassFilter.setContourAttackTime(time);
    };


    /** @inheritDoc */
    Synthesizer.prototype.setLpContourDecayTime = function(time) {
	this.lowPassFilter.setContourDecayTime(time);
    };


    /** @inheritDoc */
    Synthesizer.prototype.setLpContourSustainLevel = function(level) {
	this.lowPassFilter.setContourSustainLevel(level);
    };


    /**
     * Fills the passed audio buffer with tones generated by oscillators.
     * @param {!AudioProcessingEvent} e The audio process event object.
     * @private
     */
    Synthesizer.prototype.fillAudioBuffer_ = function(e) {
	// NOTE: We fill oscillator 2 first since it generates a modulation control
	// signal on which oscillators 0 and 1 depend.
	this.oscillators[2].fillAudioBuffer(
	    e, null, OscillatorInterface.FillMode.CLOBBER);
	this.oscillators[0].fillAudioBuffer(
	    e, this.oscillators[2].modulatorSignal,
	    OscillatorInterface.FillMode.ADD);
	this.oscillators[1].fillAudioBuffer(
	    e, this.oscillators[2].modulatorSignal,
	    OscillatorInterface.FillMode.MIX, this.oscillators.length);
    };


    /** @inheritDoc */
    Synthesizer.prototype.setVolume = function(volume) {
	this.volume_ = volume;
	this.volumeNode_.gain.value = volume;
    };


    /** @override */
    Synthesizer.prototype.getSourceNode = function() {
	return this.jsAudioNode_;
    };


    /** @override */
    Synthesizer.prototype.connect = function(destination) {
	this.analyserNode.connect(destination);
    };


    /** @override */
    Synthesizer.prototype.disconnect = function() {
	this.analyserNode.disconnect();
    };

    
    
    /**
     * A wide band pass filter designed to filter out low and high frequencies that
     * might damage poorly made speakers connected to poorly made sound cards.
     * @param {!AudioContext} audioContext Audio context to which this oscillator
     *     will be bound.
     * @param {number} lowCutoffFrequency The cutoff frequency below which sound is
     *     attenuated.
     * @param {number} highCutoffFrequency The cutoff frequency above which sound is
     *     attenuated.
     * @constructor
     * @implements {CompoundAudioNode}
     */
    var WideBandPassFilter = function(
	audioContext, lowCutoffFrequency, highCutoffFrequency) {
	/**
	 * The low pass filter Web Audio node.
	 * @type {!BiquadFilterNode}
	 * @private
	 */
	this.lowPassFilterNode_ = audioContext.createBiquadFilter();
	this.lowPassFilterNode_.type = 'lowpass';
	this.lowPassFilterNode_.frequency.value = highCutoffFrequency;

	/**
	 * The high pass filter Web Audio node.
	 * @type {!BiquadFilterNode}
	 * @private
	 */
	this.highPassFilterNode_ = audioContext.createBiquadFilter();
	this.highPassFilterNode_.type = 'highpass';
	this.highPassFilterNode_.frequency.value = lowCutoffFrequency;
	
	this.analyserNode_ = audioContext.createAnalyser();

	this.lowPassFilterNode_.connect(this.highPassFilterNode_);
	this.highPassFilterNode_.connect(this.analyserNode_);
    };


    /** @inheritDoc */
    WideBandPassFilter.prototype.getSourceNode = function() {
	return this.lowPassFilterNode_;
    };


    /** @inheritDoc */
    WideBandPassFilter.prototype.connect = function(destination) {
	this.analyserNode_.connect(destination);
    };


    /** @inheritDoc */
    WideBandPassFilter.prototype.disconnect = function() {
	this.analyserNode_.disconnect();
    };
    
    WideBandPassFilter.prototype.getAnalyserNode = function() {
	return this.analyserNode_;
    };
    
    
    
    
    /**
     * The Moog doodle manager.
     * @constructor
     */
    var Moog = function() {
	/**
	 * The set of available synthesizers.
	 * @type {!Array.<!SynthesizerInterface>}
	 * @private
	 */
	this.synthesizers_ = [];

	/**
	 * A reference to a master mixer instance.
	 * @type {!MasterMixerInterface}
	 * @private
	 */
	this.masterMixer_;

	/**
	 * @private
	 */
	this.isWebAudioEnabled_ = typeof AudioContext === 'function' || typeof webkitAudioContext === 'function';
	if (typeof webkitAudioContext === 'function') AudioContext = webkitAudioContext;

	if (this.isWebAudioEnabled_) {
	    this.initializeWebAudio_();
	}
    };

    /** @inheritDoc */
    Moog.prototype.dispose = function() {
	this.turnOffAudio();
    };


    /**
     * Initializes the Moog doodle on WebAudio-compatible browsers.
     * @private
     */
    Moog.prototype.initializeWebAudio_ = function() {
	try {
	    // Audio context creation can throw an error if the user is missing sound
	    // drivers, etc.
	    var audioContext = new AudioContext();
	} catch(e7) {
	    // Abort the initialization sequence like we do with flash.
	    return;
	}
	var oscillators = [
	    new Oscillator(
		audioContext, 0.46, OscillatorInterface.WaveForm.SQUARE,
		OscillatorInterface.Range.R16, 0, true, false, false, 0,
		true, 0.05, 0, 0.4, 1),
	    new Oscillator(
		audioContext, 0.82, OscillatorInterface.WaveForm.SQUARE,
		OscillatorInterface.Range.R4, 0, true, false, false, 0,
		true, 0.05, 0, 0.4, 1),
	    new Oscillator(
		audioContext, 0.46, OscillatorInterface.WaveForm.SQUARE,
		OscillatorInterface.Range.R32, 0, true, false, true, 0.6,
		true, 0.05, 0, 0.4, 1)
	];
	var lowPassFilter = new LowPassFilter(
	    audioContext, 2100, 7, 0, .8, 0);
	this.synthesizers_.push(new Synthesizer(
	    audioContext, oscillators, lowPassFilter, 1.0));

	var wideBandPassFilter = new WideBandPassFilter(
	    audioContext, 20, 20000);
	this.masterMixer_ = new MasterMixer(
	    audioContext, this.synthesizers_, wideBandPassFilter);
    };


    /**
     * Turns on the audio pipeline.
     */
    Moog.prototype.turnOnAudio = function() {
	if (this.masterMixer_) {
	    this.masterMixer_.turnOn();
	}
    };


    /**
     * Turns off the audio pipeline.
     */
    Moog.prototype.turnOffAudio = function() {
	if (this.masterMixer_) {
	    this.masterMixer_.turnOff();
	}
    };
    if (window) window.doodle = { moog: { Moog: Moog}};
}());
